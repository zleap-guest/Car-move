# Car-move
Basic code to move a car across the stage. 

This tutorial will go through how to create a smooth moving car across a background.

Firstly select a background.  I am using the one below as the area at the bottom provides a nice area to drive our car.

![background1](https://github.com/zleap/Car-move/blob/master/car-move1.png)


We also need a sprite

![car-sprite](https://github.com/zleap/Car-move/blob/master/car-sprite1.png)

And finally some code to move the car. 

![code-block](https://github.com/zleap/Car-move/blob/master/carmove2.png)

So we start by moving the car to where we want to start driving. The animation just moves the car across the screen, then repeats.  

The key here is getting your sprite so it looks like it is on the ground and not just driving above the ground.  


Created by Paul Sutton [http://www.zleap.net](http://www.zleap.net) on January 6th 2019

![cc-logo](https://github.com/zleap/Car-move/blob/master/88x31.png)

[Download](https://github.com/zleap/Car-move/blob/master/car-drive.sb3) project file
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzMzE0MjQxNTddfQ==
-->